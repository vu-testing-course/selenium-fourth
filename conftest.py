import pytest
from selenium import webdriver

@pytest.fixture(scope = "class", autouse = True)
def browser():
    browser = webdriver.Chrome()
    yield browser
    browser.quit()
