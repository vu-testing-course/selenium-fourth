from selenium.webdriver.common.by import By

class Test_Registration1():
    def get_link(self):
        return "http://suninjuly.github.io/registration1.html";

    def test_check_firstName_placeholder_value(self, browser):
        browser.get(self.get_link())

        expected = "Input your first name"
        actual = browser.find_element(By.CSS_SELECTOR, ".first_block > div:nth-child(1) > input:nth-child(2)").get_attribute("placeHolder")
        
        assert expected == actual, "Should be equal"